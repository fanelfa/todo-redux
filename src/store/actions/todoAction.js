export const GET_TODOS = "GET_TODOS";
export const SET_TODOS = "SET_TODOS";
export const FAIL_GET_TODOS = "FAIL_GET_TODOS";
export const ADD_TODO = "ADD_TODO";
export const UPDATE_TODO = "UPDATE_TODO";
export const DELETE_TODO = "DELETE_TODO";

export const fetchTodo = () => (dispatch) => {
  dispatch({ type: GET_TODOS });
  fetch("https://virtserver.swaggerhub.com/hanabyan/todo/1.0.0/to-do-list")
    .then((res) => res.json())
    .then((todos = []) => {
      dispatch({
        type: SET_TODOS,
        payload: { todos },
      });
    })
    .catch((error) => {
      console.log(error);
      dispatch({ type: FAIL_GET_TODOS, payload: { message: error } });
    });
};

export const addTodo =
  (todo = {}) =>
  (dispatch) => {
    dispatch({ type: ADD_TODO, payload: { todo } });
  };

export const updateTodo =
  (id, data = {}) =>
  (dispatch) => {
    dispatch({ type: UPDATE_TODO, payload: { id, data } });
  };

export const deleteTodo = (id) => (dispatch) => {
  dispatch({ type: DELETE_TODO, payload: { id } });
};
