import {
  ADD_TODO,
  DELETE_TODO,
  FAIL_GET_TODOS,
  GET_TODOS,
  SET_TODOS,
  UPDATE_TODO,
} from "../actions/todoAction";

export const initProcess = {
  isLoading: false,
  isError: null,
  message: null,
};

const initialState = {
  todos: [],
  getTodosProcess: initProcess,
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_TODOS:
      return {
        ...state,
        getTodosProcess: { ...initProcess, isLoading: true },
      };
    case SET_TODOS:
      return {
        ...state,
        todos: payload.todos || [],
        getTodosProcess: {
          ...initProcess,
          isError: false,
        },
      };
    case FAIL_GET_TODOS:
      return {
        ...state,
        getTodosProcess: {
          ...initProcess,
          isError: true,
          message: payload.message,
        },
      };
    case ADD_TODO:
      return {
        ...state,
        todos: [...state.todos, payload.todo],
      };
    case UPDATE_TODO: {
      return {
        ...state,
        todos: state.todos.map((it) => {
          if (it.id === payload.id) {
            return {
              ...it,
              ...payload.data,
            };
          }
          return it;
        }),
      };
    }
    case DELETE_TODO: {
      return {
        ...state,
        todos: state.todos.filter((it) => it.id !== payload.id),
      };
    }
    default:
      return state;
  }
};

export default reducer;
