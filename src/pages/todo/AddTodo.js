import React from "react";
import { connect } from "react-redux";
import Form from "./Form";

import { addTodo } from "../../store/actions/todoAction";
import Card from "../../components/Card";

const AddTodo = ({ addTodo = () => {} }) => {
  const onAdd = (data) => {
    addTodo(data);
  };
  return (
    <Card title="Add Todo">
      <Form formTitle="Add Todo" onSubmit={onAdd} />
    </Card>
  );
};

export default connect(null, { addTodo })(AddTodo);
