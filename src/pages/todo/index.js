import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import PropTypes from "prop-types";

import AddTodo from "./AddTodo";
import ListTodo from "./ListTodo";

import { fetchTodo } from "../../store/actions/todoAction";
import moment from "moment";

const sortByDate =
  (desc = false) =>
  (a, b) => {
    if (desc)
      return moment(a.createdAt).valueOf() - moment(b.createdAt).valueOf();
    return moment(b.createdAt).valueOf() - moment(a.createdAt).valueOf();
  };

const Todo = ({ fetchTodo, todos = [] }) => {
  const [doneTodo, setDoneTodo] = useState();
  const [notDoneTodo, setNotDoneTodo] = useState();

  useEffect(() => {
    fetchTodo();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (todos?.length > 0) {
      const sortAsc = sortByDate();
      const sortDesc = sortByDate(true);

      const done = todos.filter((it) => it.status === 1).sort(sortDesc);
      const notDone = todos.filter((it) => it.status === 0).sort(sortAsc);

      setDoneTodo(done || []);
      setNotDoneTodo(notDone || []);
    }
  }, [todos]);

  return (
    <div className="container mt-5">
      <div className="columns">
        <div className="column is-3">
          <AddTodo />
        </div>
        <div className="column">
          <ListTodo title="Not Done" todos={notDoneTodo} />
        </div>
        <div className="column">
          <ListTodo title="Done" todos={doneTodo} />
        </div>
      </div>
    </div>
  );
};

Todo.propTypes = {
  fetchTodo: PropTypes.func.isRequired,
  todos: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  todos: state.todos.todos,
});

export default connect(mapStateToProps, { fetchTodo })(Todo);
