import React, { useRef } from "react";
import { connect } from "react-redux";

import { updateTodo, deleteTodo } from "../../store/actions/todoAction";

const ItemTodo = ({
  id,
  title = "",
  isDone = false,
  updateTodo = () => {},
  deleteTodo = () => {},
  onClickEdit = () => {},
}) => {
  const actionsRef = useRef();
  const showActions = () => {
    actionsRef.current.classList.remove("is-hidden");
  };
  const hideActions = () => {
    actionsRef.current.classList.add("is-hidden");
  };
  return (
    <div
      className="box p-3"
      onMouseOver={showActions}
      onMouseLeave={hideActions}
    >
      <div className="columns is-align-items-center">
        <div className="column is-narrow">
          <input
            className="ml-1"
            type="checkbox"
            onChange={(e) =>
              updateTodo(id, { status: e.target.checked ? 1 : 0 })
            }
            checked={isDone}
          />
        </div>
        <div className="column">
          <div className="is-h3">{title}</div>
        </div>
        <div className="column is-narrow is-hidden" ref={actionsRef}>
          <button className="button is-warning is-small" onClick={onClickEdit}>
            <span className="icon is-small">
              <i className="fa fa-pencil"></i>
            </span>
          </button>
          {!isDone && (
            <button
              className="button is-danger is-small ml-1"
              onClick={() => deleteTodo(id)}
            >
              <span className="icon is-small">
                <i className="fa fa-trash"></i>
              </span>
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default connect(null, { updateTodo, deleteTodo })(ItemTodo);
