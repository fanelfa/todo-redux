import React, { useState } from "react";
import EditTodo from "./EditTodo";
import ItemTodo from "./ItemTodo";

const ListTodo = ({ title = "Done", todos = [] }) => {
  const [selectedTodo, setSelectedTodo] = useState(null);

  return (
    <div className="card is-full">
      <header className="card-header">
        <p className="card-header-title">{title}</p>
      </header>
      <div className="card-content">
        <div className="content">
          {todos.map((it = {}) => (
            <ItemTodo
              id={it.id}
              title={it.title}
              isDone={it.status === 1 ? true : false}
              key={it.id}
              onClickEdit={() => setSelectedTodo(it)}
            />
          ))}
        </div>
      </div>
      <EditTodo onClose={() => setSelectedTodo(null)} todo={selectedTodo} />
    </div>
  );
};

export default ListTodo;
