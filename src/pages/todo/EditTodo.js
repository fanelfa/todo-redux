import React from "react";
import { connect } from "react-redux";
import Card from "../../components/Card";
import Modal from "../../components/Modal";
import Form from "./Form";

import { updateTodo, deleteTodo } from "../../store/actions/todoAction";

const EditTodo = ({
  todo = {},
  onClose = () => {},
  updateTodo = () => {},
  deleteTodo = () => {},
}) => {
  const onUpdate = (data) => {
    if (typeof data === "object") {
      updateTodo(todo.id, data);
      onClose();
    }
  };
  const onDelete = () => {
    deleteTodo(todo.id);
    onClose();
  };

  return (
    <Modal visible={todo !== null} onClose={onClose}>
      <Card title="Edit Todo">
        <Form dataForm={todo} onSubmit={onUpdate} onDelete={onDelete} />
      </Card>
    </Modal>
  );
};

export default connect(null, { updateTodo, deleteTodo })(EditTodo);
