import React, { useEffect, useState } from "react";

import { v4 } from "uuid";
import moment from "moment";

const Form = ({ dataForm, onSubmit = () => {}, onDelete = () => {} }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isDone, setIsDone] = useState(false);

  const _onSubmit = (e) => {
    e.preventDefault();
    const body = {
      id: v4(),
      title,
      description,
      status: isDone ? 1 : 0,
      createdAt: moment().format("YYYY-MM-DD HH:mm"),
    };
    onSubmit(body);

    setTitle("");
    setDescription("");
    setIsDone(false);
  };

  useEffect(() => {
    if (typeof dataForm === "object") {
      setTitle(dataForm.title);
      setDescription(dataForm.description);
      setIsDone(dataForm.status === 1);
    }
  }, [dataForm]);

  return (
    <form className="content" onSubmit={_onSubmit}>
      <div className="field">
        <label className="label">Title</label>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="Text input"
            onChange={(e) => setTitle(e.target.value)}
            value={title}
            required
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Description</label>
        <div className="control">
          <textarea
            className="textarea"
            placeholder="Textarea"
            onChange={(e) => setDescription(e.target.value)}
            value={description}
          ></textarea>
        </div>
      </div>
      <div className="field">
        <div className="control">
          <label className="checkbox">
            <input
              type="checkbox"
              onChange={(e) => setIsDone(e.target.checked)}
              checked={isDone}
            />
            <span className="pl-2">Selesai</span>
          </label>
        </div>
      </div>
      <div className="field is-grouped is-grouped-right mt-5">
        {dataForm && dataForm.status !== 1 && (
          <div className="control">
            <button className="button is-danger" onClick={onDelete}>
              Delete
            </button>
          </div>
        )}
        <div className="control">
          <button type="submit" className="button is-success">
            Submit
          </button>
        </div>
      </div>
    </form>
  );
};

export default Form;
