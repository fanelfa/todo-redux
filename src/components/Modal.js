import React from "react";

const Modal = ({ visible = false, onClose = () => {}, children }) => {
  if (!visible) return null;
  return (
    <div className={`modal${visible ? " is-active" : ""}`}>
      <div className="modal-background" onClick={onClose}></div>
      {children}
    </div>
  );
};

export default Modal;
